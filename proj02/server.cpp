#include "server.h"
#include "function.h"

using namespace std;

int main(int argc, char *argv[]) {

	// Nastaveni odchytavani signalu pro necekane ukonceni serveru
	struct sigaction action;
	action.sa_handler = signalHandler;
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0;

	// Odychytavani signalu SIGTERM & SIGINT
	sigaction(SIGTERM, &action, NULL);
	sigaction(SIGINT, &action, NULL);

	int port = -1;
	int opt;
	bool portFound = false;
	while ((opt = getopt(argc, argv, "p:")) != -1) {
		switch (opt) {
		case 'p':
			if (portFound)
				printError("Duplicitni parametr");
			port = atoi(optarg);
			portFound = true;
			break;
		default:
			printError("Neznamy parametr");
		}
	}
	if (!portFound)
		printError("Program spusteny bez parametru");

	// Vytvoreni socketu
	int mySocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mySocket == -1)
		printError("SERVER: Chyba ve funkci socket()");

	// Nastaveni vytvoreneho socketu
	struct sockaddr_in socketAddres;
	socketAddres.sin_family = AF_INET;
	socketAddres.sin_port = htons(port);
	socketAddres.sin_addr.s_addr = INADDR_ANY;
	if (bind(mySocket, (struct sockaddr *) &socketAddres, sizeof(socketAddres))
			== -1)
		printError("SERVER: Chyba ve funkci bind()");

	// Maximalne 10 spojeni
	if (listen(mySocket, 10) == -1)
		printError("SERVER: Chyba ve vytvoreni fronty spojeni");

	socklen_t addrLength;
	do {
		ThreadData *data = new ThreadData;

		addrLength = sizeof(data->client);

		int client = accept(mySocket, (sockaddr *) &(data->client),
				&addrLength);
		data->partner = client;

		if (signalON)
			break;

		if (client == -1)
			printError("SERVER: Chyba v prijimanim spojeni");

		pthread_t thread;

		if (pthread_create(&thread, NULL, newConnection, (void *) data) != 0)
			printError("CSERVER: hyba pri vytvareni vlakna");
		delete[] data;
	} while (!signalON);

	close(mySocket);
	pthread_exit (NULL);
	return (0);
}

void signalHandler(int param) {
	signalON = true;
}

void *newConnection(void *data) {
	ThreadData *thread = (ThreadData *) data;

	RESULT result = getData(thread->partner, true);
	if (result.header.length() > 0) {
		if (result.header.find("ACTION UPLOAD") == 0) {
			upload(thread, result);
		} else if (result.header.find("ACTION DOWNLOAD") == 0) {
			download(thread, result);
		} else
			sendData(thread->partner, "ERROR - neplatny pozadavek\r\n\r\n");
	} else
		sendData(thread->partner, "ERROR - neplatny pozadavek\r\n\r\n");

	close(thread->partner);
	pthread_exit (NULL);
}

void upload(ThreadData *thread, RESULT result) {
	size_t pos;
	string filename;

	// Klient chce neco uploadovat
	result.header.erase(0, string("ACTION UPLOAD ").length());
	pos = result.header.find("\r\n");

	// Ziskani z hlavicky jmena uploadovaneho souboru
	filename = result.header.substr(0, pos);
	result.header.erase(0, filename.length() + 2);

	// Pokud uploadovany souber neprepise serverovsky program tak je vse OK
	if (filename == "server") {
		sendData(thread->partner,
				"ERROR - pokousite se prepsat serverovy program");
		close(thread->partner);
		pthread_exit (NULL);
		return;
	}

	// Vytvoreni docasneho souboru
	stringstream tempFilename;
	tempFilename << thread->partner << TEMPFILE_SERVER;
	ofstream outFile(tempFilename.str().c_str());
	if (outFile.is_open()) {
		do {
			outFile << result.data;
			result = getData(thread->partner, false);
		} while (result.data.length() > 0);
		outFile.close();

		// Odstaraneni puvodniho souboru se stejnym nazvem
		remove(filename.c_str());
		// Prejmenovani docasneho souboru na "trvaly"
		rename(tempFilename.str().c_str(), filename.c_str());
	} else {
		sendData(thread->partner,
				"ERROR - nepodarilo se vytvorit docasny soubor");
		printError("SERVER: Chyba nepodarilo se vytvorit docasny soubor");
	}
}

void download(ThreadData *thread, RESULT result) {
	string filename;
	long filesize;

	// Klient chce neco stahnout ze serveru
	result.header.erase(0, string("ACTION DOWNLOAD ").length());
	filename = result.header;

	// Otevreni souboru na serveru
	ifstream f;
	f.open(filename.c_str());
	if (f.is_open()) {

		// Ziskani velikosti souboru
		f.seekg(0, ios::end);
		filesize = f.tellg();
		f.seekg(0, ios::beg);

		// Odeslani informaci o pozadovanem souboru
		ostringstream size;
		size << filesize;
		sendData(thread->partner,
				"FILENAME " + filename + "\r\nFILESIZE " + size.str()
						+ "\r\n\r\n");

		char *outBuffer = new char[BUFFER_SIZE];
		streamsize readsize;
		while (!f.eof()) {
			f.read(outBuffer, BUFFER_SIZE);
			readsize = f.gcount();

			// Odesilani obsahu souboru
			sendData(thread->partner, string(outBuffer, readsize));
			memset(outBuffer, 0, BUFFER_SIZE);
		}
	} else
		sendData(thread->partner, "ERROR - SERVER->Neexistujici souboru\r\n\r\n");
}
