#include <iostream>
#include <stdlib.h>
#include <string>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

using namespace std;

#ifndef FUNCTION_H_
#define FUNCTION_H_

/**
 * Velikost bufferu a maximalni velikost prenasenych dat
 */
#define BUFFER_SIZE 4096

/**
 * Struktura pro ulozeni prijatych dat
 * header - hlavicka
 * data - zbytek prijatych dat bez hlavicky
 */
typedef struct {
	string header;
	string data;
} RESULT;

/**
 * Struktura pro ulozeni informaci o klientovi/serveru
 */
typedef struct {
	sockaddr_in client;
	int partner;
} ThreadData;

/**
 * Funkce pro vypis zprav na STDERR
 * @param string msg - Text zpravy
 */
void printError(string msg);

/**
 * Odesle data na daneho partnera (client/server)
 * @param partner int - Pripojeny partner
 * @param msg string - Data k odeslani
 * @return ssize_t
 */
ssize_t sendData(int client, string msg);

/**
 * Ziska data od partnera
 * @param partner int - Pripojeny partner
 * @param getHeader bool - Ma odedelit hlavicku od dat ANO = true / NE = false
 * @return RESULT
 */
RESULT getData(int partner, bool getHeader);

#endif /* FUNCTION_H_ */
