#include "function.h"

ssize_t sendData(int partner, string msg) {
	ssize_t size;

	// Odesilani dat
	if ((size = send(partner, msg.c_str(), msg.length(), 0)) == -1)
		printError("Chyba pri odesilani dat");
	return (size);
}

void printError(string msg) {
	cerr << msg << endl;
	exit (EXIT_FAILURE);
}

RESULT getData(int partner, bool needHeader) {
	RESULT result;

	// Vytvoreni bufferu pro ulozeni dat
	char buffer[BUFFER_SIZE];
	int size;
	do {
		if ((size = recv(partner, buffer, BUFFER_SIZE - 1, 0)) == -1)
			printError("Chyba pri nacitani dat ze socketu");

		if (size == 0)
			return (result);
		buffer[size] = '\0';
		result.data.append(string(buffer));

		if (needHeader) {
			size_t pos = result.data.find("\r\n\r\n");
			if (pos != string::npos) {
				result.header = result.data.substr(0, pos);
				result.data.erase(0, pos + 4);
			}
		}
	} while (needHeader && (result.header.length() == 0));
	return (result);
}
