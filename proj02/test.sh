#!/bin/sh

printf "Sputen simulacni skript IPK 2016\n"

make all

printf "\nRUN: ./server -p 4242 &\n"
./server -p 4242 &
echo "SERVER: STARTING"
SERVER=$!
sleep 1
printf "\nSERVER: RUN (PID=%s PORT=4242)\n" "$SERVER"

echo "Client zkousi uploadovat soubor testFile.txt"
printf "RUN: ./client -p 4242 -h localhost -u testFile.txt\n"
./client -p 4242 -h localhost -u testFile.txt 
RESULT=$?
if [ 0 -eq $RESULT ]; then
	printf "\tUSPECH\n"
else 
	printf "\tNEUSPECH\n"
	kill $SERVER
	exit 1
fi
sleep 1

echo "Client zkousi stahnout soubor testFile.txt"
printf "RUN: ./client -p 4242 -h localhost -d testFile.txt\n"
./client -p 4242 -h localhost -u testFile.txt
RESULT=$?
if [ 0 -eq $RESULT ]; then
	printf "\tUSPECH\n"
else 
	printf "\tNEUSPECH\n"
	kill $SERVER
	exit 1
fi
sleep 1

echo "Client zkousi stahnout neexistujici soubor errorFile"
printf "RUN: ./client -p 4242 -h localhost -d errorFile\n"
./client -p 4242 -h localhost -u errorFile
RESULT=$?
if [ 0 -eq $RESULT ]; then
	printf "\tUSPECH\n"
else 
	printf "\tNEUSPECH\n"
	kill $SERVER
	exit 1
fi
sleep 1

kill $SERVER
exit 0
