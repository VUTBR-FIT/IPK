FIT - Počítačové komunikace a sítě (IPK)
Autor: Lukáš Černý (xcerny63)
-----------------------------------------------------------------
Vytvořené program slouží pro přenos
souborů mezi serverem a klientem.

Návod k použití:
	1) Přeložit
	$> make all

	2) Na straně serveru spustit serverovský program "server" a vybrat port pro komunikaci (PORT): server -p PORT
	$> server -p 4242

	3) Na straně clienta spustit klientský program "client", zadat server (SERVER), na kterém běží serverovský program , port pro komunikaci (PORT), a zadat parametr pro režim -d (= download, klient stahuje ze serveru) nebo -u (= upload, klient bude odesílat soubor) a zadat port na kterém komunikuje server: client -p PORT -h SERVER -u|-d FILE

	$> client -p 4242 -h localhost -u|-d testFile.txt
	

