#include "client.h"
#include "function.h"

using namespace std;

int main(int argc, char **argv) {
	int port;
	string host;
	char method;
	string filename;

	bool flagPort = false;
	bool flagMethod = false;
	bool flagHost = false;
	int opt;
	while ((opt = getopt(argc, argv, "p:h:u:d:")) != -1) {
		switch (opt) {
		case 'p':
			if (flagPort)
				printError("Duplicitni parametr");
			port = atoi(optarg);
			flagPort = true;
			break;
		case 'h':
			if (flagHost)
				printError("Duplicitni parametr");
			host = string(optarg);
			flagHost = true;
			break;
		case 'u':
			if (flagMethod)
				printError("Duplicitni parametr");
			method = 'u';
			flagMethod = true;
			filename = string(optarg);
			break;
		case 'd':
			if (flagMethod)
				printError("Duplicitni parametr");
			method = 'd';
			flagMethod = true;
			filename = string(optarg);
			break;
		default:
			printError("Neznamy parametr");
			break;
		}
	}
	if (flagPort == false or flagMethod == false or flagHost == false)
		printError("Spatne zadane parametry");

	struct hostent *connection = gethostbyname(host.c_str());
	if (connection == NULL)
		printError("CLIENT: Chyba ve funkci gethostbyname()");

	int mySocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mySocket == -1)
		printError("CLIENT: Chyba ve funkci socket()");

	struct sockaddr_in serverAddres;
	serverAddres.sin_port = htons(port);
	serverAddres.sin_family = AF_INET;
	memcpy(&(serverAddres.sin_addr), connection->h_addr, connection->h_length);

	// Pripojeni
	if (connect(mySocket, (struct sockaddr *) &serverAddres,
			sizeof(serverAddres)) == -1)
		printError("CLIENT: Chyba pripojeni");

	if (method == 'u')
		upload(mySocket, filename);
	if (method == 'd')
		download(mySocket, filename);
	close(mySocket);
	return (0);
}

void download(int sock, string filename) {
	// Odeslani pozadavku o stazeni souboru
	sendData(sock, "ACTION DOWNLOAD " + filename + "\r\n\r\n");

	// Ziskani odpovedi serveru
	RESULT result = getData(sock, true);

	// Zda nenastala chyba, pokud ano tak se vypise a ukonci program
	if (result.header.find("ERROR") == 0) {
		result.header.erase(0, string("ERROR ").length());
		printError(result.header);
	}

	result.header.erase(0, string("FILENAME ").length());
	int pos = result.header.find("\r\n");
	if (result.header.substr(0, pos) != filename)
		printError(
				"CLIENT: Chyba pri prijimani souboru (nazvy prijimaneho a pozadovaneho souboru se neshoduji)");
	else
		result.header.erase(0, filename.length() + 2);

	result.header.erase(0, string("FILESIZE ").length());
	pos = result.header.find("\r\n\r\n");

	long filesize = atol(result.header.substr(0, pos).c_str());
	long readsize = 0;
	ofstream outFile(TEMPFILE_CLIENT);
	if (outFile.is_open()) {
		do {
			outFile << result.data;
			readsize += result.data.length();
			result = getData(sock, false);
		} while (result.data.length() > 0);
		outFile.close();

		if (readsize != filesize) {
			cerr << "CLIENT: Prenos souboru selhal" << endl;
			remove (TEMPFILE_CLIENT);
		} else {
			remove(filename.c_str());
			rename(TEMPFILE_CLIENT, filename.c_str());
		}
	} else {
		printError("CLIENT: Chyba pri vytvoreni docasneho souboru");
	}
}

void upload(int sock, string filename) {
	ifstream f;
	f.open(filename.c_str());
	if (f.is_open()) {
		// Zjisteni velikosti souboru
		f.seekg(0, ios::end);
		ssize_t filesize = f.tellg();
		f.seekg(0, ios::beg);

		ssize_t sent = 0;

		// Odeslani pozadavku pro upload
		sendData(sock, "ACTION UPLOAD " + filename + "\r\n\r\n");

		char *outBuffer = new char[BUFFER_SIZE];
		streamsize readsize;
		while (!f.eof()) {
			f.read(outBuffer, BUFFER_SIZE);
			readsize = f.gcount();
			sent += readsize;

			// Odesilani souboru (pokud je velky tak po castech)
			sendData(sock, string(outBuffer, readsize));
			memset(outBuffer, 0, BUFFER_SIZE);
		}
		delete[] outBuffer;

		if (sent != filesize) {
			cerr << "CLIENT: Chyba odeslala se spatna velikost" << endl;
		}
	} else
		printError("CLIENT: Chyba pri otevreni souboru");
}
