#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <cstring>
#include <netinet/in.h>     //struct sockaddr_in, htons
#include <iostream> //cerr
#include <sys/socket.h> // bind, socket, socklen_t
#include <sys/types.h> // bind, socket
#include <netdb.h>
#include <sys/socket.h> // socklen_t
#include <sstream>//ostringstream
#include <unistd.h> // getopt
#include <fstream>

using namespace std;

#ifndef CLIENT_H_
#define CLIENT_H_

/*
 * Jmeno docasneho souboru
 */
#define TEMPFILE_CLIENT "~client.tmp"

/**
 * Stahne soubor ze serveru
 * @param server int - Vytvoreny socket
 * @param filename string - Jmeno souboru pro vytvoreni
 */
void download(int server, string filename);

/**
 * Nahraje soubor na server
 * @param server int - Vytvoreny socket
 * @param filename string - Jmeno souboru pro vytvoreni
 */
void upload(int server, string filename);

#endif /* CLIENT_H_ */
