#include <stdlib.h>
#include <string>
#include <cerrno>
#include <cstring>
#include <netinet/in.h>     //struct sockaddr_in, htons
#include <iostream> //cerr
#include <sys/types.h> // bind, socket
#include <fstream>
#include <signal.h> // struct sigaction
#include <sys/socket.h> // ind, socket, socklen_t
#include <unistd.h> // getopt
#include <arpa/inet.h> // htons
#include <pthread.h>
#include <sstream>//ostringstream
#include "function.h"

using namespace std;

#ifndef SERVER_H_
#define SERVER_H_

/**
 * Jmeno docasneho souboru
 */
#define TEMPFILE_SERVER "~tempServer.tmp"

bool signalON = false;

/**
 * Vytvori vlakno
 * @param data void - obsahuje informace o pripojenem klientovi
 */
void *newConnection(void *data);

/**
 * Odesle soubor klientovi
 * @param thread ThreadData - struktara obsahujici informace o pripojenem klientovi
 * @param result RESULT - obsahuje prvni zpravu od klienta
 */
void upload(ThreadData *thread, RESULT result);

/**
 * Ulozi soubor prijimany od klienta
 * @param thread ThreadData - struktara obsahujici informace o pripojenem klientovi
 * @param result RESULT - obsahuje prvni zpravu od klienta
 */
void download(ThreadData *thread, RESULT result);

/**
 * Zachytavani signalu
 * @param signal int
 */
void signalHandler(int signal);

#endif /* SERVER_H_ */
