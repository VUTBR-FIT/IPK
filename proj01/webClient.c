#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <ctype.h>
#include <netdb.h>
#include <netinet/in.h> 	//struct sockaddr_in
#include <unistd.h> 		// close()
#include <stdbool.h>
#include <sys/socket.h> // bind, socket, socklen_t
#include <sys/types.h> // bind, socket
#include "webClient.h"


int main(int argc, char *argv[]){
	int error = E_ALL_OK;
	//mbstowcs(NULL,s,0)
	if (argc != 2){
		CALL_ERROR(E_BAD_PARAMS);
	} else {
		URL urlData;
		char *copystring;
		char *copyFilename;
		if ((copystring = (char *) malloc(sizeof(char) * (strlen(argv[1]) + 1))) == NULL){
			CALL_ERROR(E_MALLOC);
		}
		memcpy(copystring, argv[1], (int) strlen(argv[1]) + 1);

		for (int i = 0; i < 5; i++){
			initUrl(&urlData);
			//printUrl(&urlData);
			error = parseUrl(copystring, &urlData);
			if (error == E_ALL_OK){
				if (i > 0){
					free(urlData.fileName);
					urlData.fileName = copyFilename;
				}
				//printUrl(&urlData);
				error = openConnection(&urlData);
				if (error == E_BAD_HTTP) error = openConnection(&urlData);
				if (error == E_REDIRECT) {
					if (i == 0){
						copyFilename = urlData.fileName;
						urlData.fileName = NULL;
					}
					free(copystring);
					copystring = NULL;
					if ((copystring = (char *) malloc(sizeof(char) * (strlen(urlData.inputUrl) + 1))) == NULL){
						CALL_ERROR(E_MALLOC);
					}
					memcpy(copystring, urlData.inputUrl, (int) strlen(urlData.inputUrl) + 1);
					cleanUrl(&urlData);
				}
				if (error == E_ALL_OK) i = 10;
			} else i = 10;
		}
		if (copystring != NULL) {
			free(copystring);
			copystring = NULL;
		}
	}
	return (error);
}

void errorPrint(errorCode code){
	if (code < 10){
		fprintf(stderr, errMessage[code],"\n");
	}
}

void initUrl(URL *url){
	url->path = NULL;
	url->server = NULL;
	url->fileName = NULL;
	url->inputUrl = NULL;
	url->port = 0;
	memcpy(url->http, "HTTP/1.1", strlen("HTTP/1.1") + 1);
	url->returnCode = 0;
	url->chunked = false;
}

void printUrl(URL *url){
	printf("PATH:%s\n", url->path);
	printf("SERVER:%s\n", url->server);
	printf("FILENAME:%s\n", url->fileName);
	printf("URL:%s\n", url->inputUrl);
	printf("PORT:%u\n", url->port);
	if (url->chunked == true) printf("CHUNKED\n");
	printf("RETURN:%ld\n", url->returnCode);
}

void cleanUrl(URL *url){
	if (url->server != NULL) free(url->server);
	if (url->path != NULL) free(url->path);
	if (url->fileName != NULL) free(url->fileName);
	if (url->inputUrl != NULL) free(url->inputUrl);

	url->server = NULL;
	url->path = NULL;
	url->fileName = NULL;
	url->inputUrl = NULL;
}

int parseUrl(char inputUrl[], URL *url){
	int state = 0;
	int pom = 6;
	int filename = 0;
	int tempLength = 0;
	unsigned int port = 0;
	for (int letter = 0; letter < (int) strlen(inputUrl); letter++) {
		switch (state) {
			case 0:
				if (inputUrl[letter] != 'h') state = -1;
				else state++;
				break;
			case 1:
				if (inputUrl[letter] != 't') state = -1;
				else state++;
				break;
			case 2:
				if (inputUrl[letter] != 't') state = -1;
				else state++;
				break;
			case 3:
				if (inputUrl[letter] != 'p') state = -1;
				else state++;
				break;
			case 4:
				if (inputUrl[letter] != ':') state = -1;
				else state++;
				break;
			case 5:
				if (inputUrl[letter] != '/') state = -1;
				else state++;
				break;
			case 6:
				if (inputUrl[letter] != '/') state = -1;
				else state++;
				break;
			case 7:
				if (isalpha(inputUrl[letter]) || isdigit(inputUrl[letter]) || (inputUrl[letter] == '.') || (inputUrl[letter] == '-')){
					if (url->server == NULL){
						tempLength = 0;
						if ((url->server = (char *) malloc((pom +1) * sizeof(char))) == NULL) {
							CALL_ERROR(E_MALLOC);
						}
					} else {
						if (tempLength && (tempLength % pom) == 0){
							if ((url->server = (char *) realloc(url->server, tempLength + pom + 1)) == NULL){
								CALL_ERROR(E_MALLOC);
							}
						}
					}
					if(inputUrl[letter] == ' ') state = -1;
					url->server[tempLength] = inputUrl[letter];
					url->server[tempLength + 1] = '\0';
					tempLength++;
					if ((int) strlen(inputUrl) == letter + 1){
						url->port = 80;
					}
				} else if (inputUrl[letter] == '/') {
					url->port = 80;
					state += 2;
				} else if (inputUrl[letter] == ':') {
					state++;
				} else if (inputUrl[letter] == ' '){
					if (strcmp(url->server, "") == 0){
						state = -1;
					} else {
						if (tempLength && (tempLength % pom) == 0){
							if ((url->server = (char *) realloc(url->server, tempLength + pom + 1)) == NULL){
								CALL_ERROR(E_MALLOC);
							}
						}
					}
					url->server[tempLength] = inputUrl[letter];
					tempLength++;
					if ((int) strlen(inputUrl) == letter + 1){
						url->port = 80;
					}
				} else {
					url->port = 80;
					state = -1;
				}
				break;
			case 8:
				if (isdigit(inputUrl[letter])){
					if ((url->port * 10 + (inputUrl[letter] - '0')) > 65535){
						CALL_ERROR(E_BAD_PORT);
					}
					port = url->port;
					url->port = port * 10 + (inputUrl[letter] - '0');
				} else if (inputUrl[letter] == '/'){
					state++;
				} else {
					CALL_ERROR(E_BAD_URL);
				}
				break;
			case 9:
				if (inputUrl[letter] == ' '){
					if (url->path == NULL){
						state = -1;
					} else {
						if (tempLength && (tempLength % pom) == 0){
							if ((url->path = (char *) realloc(url->path, tempLength + pom + 1)) == NULL){
								CALL_ERROR(E_MALLOC);
							}
						}
						url->path[tempLength] = '%';
						tempLength++;
						if (tempLength && (tempLength % pom) == 0){
							if ((url->path = (char *) realloc(url->path, tempLength + pom + 1)) == NULL){
								CALL_ERROR(E_MALLOC);
							}
						}
						url->path[tempLength] = '2';
						tempLength++;
						if (tempLength && (tempLength % pom) == 0){
							if ((url->path = (char *) realloc(url->path, tempLength + pom + 1)) == NULL){
								CALL_ERROR(E_MALLOC);
							}
						}
						url->path[tempLength] = '0';
						tempLength++;
					}
				} else if (isalpha(inputUrl[letter])
						|| isdigit(inputUrl[letter])
						|| (inputUrl[letter] == '.')
						|| (inputUrl[letter] == '/')
						|| (inputUrl[letter] == '-')
						|| (inputUrl[letter] == '_')){
					if (inputUrl[letter] == '/') filename = 0;
					else filename = 1;
					if (url->path == NULL){
						tempLength = 1;
						if ((url->path = (char *) malloc((pom + 1) * sizeof(char))) == NULL) {
							CALL_ERROR(E_MALLOC);
						}
						url->path[0] = '/';
					} else {
						if (tempLength && (tempLength % pom) == 0){
							if ((url->path = (char *) realloc(url->path, tempLength +  pom + 1)) == NULL){
								CALL_ERROR(E_MALLOC);
							}
						}
					}
					url->path[tempLength] = inputUrl[letter];
					url->path[tempLength +1] = '\0';
					tempLength += (int) sizeof(char);
				} else {
					CALL_ERROR(E_BAD_URL);
				}
				break;
			default:
				CALL_ERROR(E_BAD_URL);
				break;
		}
	}
	if (url->path == NULL){
		if ((url->path = (char *) malloc(sizeof(char) * 2)) == NULL){
			CALL_ERROR(E_MALLOC);
		}
		url->path[0] = '/';
		url->path[1] = '\0';
		filename = 0;
	}
	if (url->fileName != NULL){
		free(url->fileName);
		url->fileName = NULL;
	}
	if (filename == 1){
		char *token;
		char * result;
		token = strtok(inputUrl, "/");
		result = token;
		while (token != NULL){
			token = strtok(NULL, "/");

			if (token != NULL){
				result = token;
			}
		}
		if ((url->fileName = (char *) malloc(sizeof(char) * (strlen(result) + 1))) == NULL){
			CALL_ERROR(E_MALLOC);
		}
		memcpy(url->fileName, result, strlen(result) + 1);
	} else {

		if ((url->fileName = (char *) malloc(strlen("index.html") + 1)) == NULL){
			CALL_ERROR(E_MALLOC);
		}
		memcpy(url->fileName, "index.html", strlen("index.html"));
		url->fileName[10] = '\0';
	}
	tempLength = 0;
	if (((int) strlen(url->server) == 0) || (url->port == 0) || (state == -1)){
		CALL_ERROR(E_BAD_URL);
	}
	if (state == -1){
		return (E_BAD_URL);
	}
	return (E_ALL_OK);
}

errorCode openConnection(URL *url){
	char *error;
	char *msgTemplate = "GET %s %s\r\nHost: %s\r\nUser-Agent: WebClient (author xcerny63)\r\nConnection: close\r\n\r\n";
	char message[1024];
	//printUrl(url);
	sprintf(message, msgTemplate, url->path, url->http, url->server);
	//printf("%s\n", message);
	struct hostent *connection = gethostbyname(url->server);
	if(connection == NULL){
		CALL_ERROR(E_HOST_CREATE);
	}

	int mySocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mySocket <= 0){
		CALL_ERROR(E_SOCKET_OPEN);
	}

	struct sockaddr_in server_addres;
	server_addres.sin_port = htons(url->port);
	server_addres.sin_family = AF_INET;
	memcpy(&(server_addres.sin_addr), connection->h_addr_list[0], connection->h_length);

	// Pripojeni
	if (connect(mySocket, (const struct sockaddr *) &server_addres, sizeof(server_addres)) != 0){
		CALL_ERROR(E_CONNECT);
	}

	// Odeslani pozadavku
	int size_send = send(mySocket, message, sizeof(message), 0);
	if (size_send < 0){
		CALL_ERROR(E_REQUEST);
	}

	FILE *file = NULL;

	int size = 0;
	int state = 0;
	char *chunked = (char *) malloc(sizeof(char)*10);
	int chunkedLen = 0;
	while ((size = recv(mySocket, message, 1024, 0)) != 0){
		for (int i = 0; i < size; i++){
			switch (state) {
				case 0:
					if (message[i] == '\r') state++;
					else state = 0;
					break;
				case 1:
					if (message[i] == '\n') state++;
					else state = 0;
					break;
				case 2:
					if (message[i] == '\r') state++;
					else state = 0;
					break;
				case 3:
					if (message[i] == '\n'){
						//printf("%s", message);
						char copyMsg[1024];
						memcpy(copyMsg, message, i+1);
						char *firstRow = strtok(copyMsg, "\r\n");
						if (strcmp(url->http, strtok(firstRow, " ")) != 0){
							memcpy(url->http, "HTTP/1.0", strlen("HTTP/1.0") + 1);
							free(chunked);
							chunked = NULL;
							return (E_BAD_HTTP);
						}
						url->returnCode = strtol(strtok(NULL, " "), &error, 10);
						state++;

						if ((url->returnCode == 302) || (url->returnCode == 301)) {
							free(chunked);
							chunked = NULL;
							memcpy(copyMsg, message, i+1);
							char *row = strtok(copyMsg, "\r\n");
							char *substring = findSubString(row, "Location:");
							while(row != NULL) {
								if ((substring != NULL) && (((int) strlen(substring) - (int) strlen(row)) < 10)){
									char *colRow = strtok(substring, " ");
									colRow = strtok(NULL, " ");
									if ((url->inputUrl = (char *) malloc(sizeof(char) * (strlen(colRow) + 1))) == NULL){
										CALL_ERROR(E_MALLOC);
									}
									memcpy(url->inputUrl, colRow, (int) strlen(colRow) + 1);
									return (E_REDIRECT);
								}
								row = strtok(NULL, "\r\n");
								if (row != NULL) substring = findSubString(row, "Location:");
							}
							break;
						} else if ((url->returnCode == 400)) {
							memcpy(url->http, "HTTP/1.0", strlen("HTTP/1.0") + 1);
							free(chunked);
							chunked = NULL;
							return (E_BAD_HTTP);
						}

						char chunkedMsg[64] = "Transfer-Encoding: chunked";
						char *result;
						if ((result = findSubString(message, chunkedMsg)) != NULL){
							//if(i > (int) strlen(result)){
								url->chunked = true;
								state = -1;
							//}
						}
					}
					else state = 0;
					break;
				case -1: // Chunked START
					if ((message[i-1] == '\r') && (message[i] == '\n')){
						chunkedLen = strtol(chunked, &error, 16);
						state--;
					} else {
						if (chunkedLen && (chunkedLen % 10) == 0){
							if ((chunked = (char *) realloc(chunked, chunkedLen +  10 + 1)) == NULL){
								CALL_ERROR(E_MALLOC);
							}
						}

						chunked[chunkedLen] = message[i];
						chunked[chunkedLen + 1] = '\0';
						chunkedLen++;
					}
					break;
				case -2:
					if (chunkedLen > 0) {
						if (file == NULL){
							file = fopen(url->fileName, "w+");
							fwrite("", 1, strlen(""), file);
						}
						fputc(message[i], file);
						chunkedLen--;
						//chunked = (char *) malloc(sizeof(char)*10);
					} else {
						i += 2;
						state = -1;
					}
					if ((message[i] == '\n') && (message[i-1] == '\r')) chunkedLen++;
					break;
				default:
					if (file == NULL){
						file = fopen(url->fileName, "w+");
						fwrite("", 1, strlen(""), file);
					}
					fputc(message[i], file);
					break;
			}
		}

	}
	if (chunked != NULL){
		free(chunked);
		chunked = NULL;
	}
	//printf("CODE: %li\n", url->returnCode);
	fclose(file);
	if (size < 0){
		CALL_ERROR(E_REQUEST_ANSWER);
	}

	close(mySocket);

	return (E_ALL_OK);

}

char  *findSubString(const char *str1, const char *str2){
	return (strstr(str1, str2));
}
