/*
 * webClient.h
 *
 *  Created on: Mar 10, 2016
 *      Author: Lukáš Černý (xcerny63)
 */

#ifndef WEBCLIENT_H_
#define WEBCLIENT_H_

#include <stdio.h>
#include <stdlib.h>

#define CALL_ERROR(err) errorPrint(err); return(err);

#define STR_BUFFER 4096

typedef struct url{
	char *server;
	char *inputUrl;
	char http[10];
	char *fileName;
	char *path;
	unsigned int port;
	long int returnCode;
	bool chunked;
} URL;

const char *errMessage[] = {
		"Vsechno v poradku%s",
		"Neplatny pocet parametru ($> webclient <server>)%s",
		"Neplatna URL adresa%s",
		"Neplatny protokol%s",
		"Neplatny rozsah portu%s",
		"Ziskani adresy serveru se nezdarilo%s"
		"Vytvoreni socketu selhalo%s",
		"Pripojeni se nezdarilo%s",
		"Chyba pozadavku%s",
		"Chyba pri odpovedi na pozadavek%s",
};
typedef enum {
	E_BAD_HTTP = -10,
	E_ALL_OK = 0,
	E_BAD_PARAMS = 1,
	E_BAD_URL = 2,
	E_BAD_PROTOCOL = 3,
	E_BAD_PORT = 4,
	E_HOST_CREATE = 5,
	E_SOCKET_OPEN = 6,
	E_CONNECT = 7,
	E_REQUEST = 8,
	E_REQUEST_ANSWER = 9,
	E_REDIRECT = 10,
	E_MALLOC
}errorCode;

void errorPrint(errorCode code);

void initUrl(URL *url);
void printUrl(URL *url);
void cleanUrl(URL *url);

int parseUrl(char *inputUrl, URL *url);

errorCode openConnection(URL *url);

char *findSubString(const char *str1, const char *str2);

#endif /* WEBCLIENT_H_ */
